FROM node:12.14-alpine3.10

RUN mkdir /src

WORKDIR /src
ADD . /src

EXPOSE 8000

RUN npm i

ENTRYPOINT ["node", "app.js"]
