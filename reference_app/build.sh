#!/bin/bash
set -e
version=$(cat package.json |jq -r '.version')
IMAGE="eu.gcr.io/best-practices-273615/bp:$version"

docker build -t bp .
docker tag bp $IMAGE
docker push $IMAGE

kubectl set image deployment/bp bp=$IMAGE
