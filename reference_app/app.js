const express = require('express')
const mysql = require('promise-mysql')
const app = express()
const port = parseInt(process.env.SERVER_PORT || 8000)

let _pool = false

const getPool = async () => {
  if (_pool !== false) {
    return _pool
  }

  _pool = await mysql.createPool({
    connectionLimit : 10,
    host : process.env.MYSQL_HOST,
    user : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DATABASE
  })

  return _pool
}

const calculate = () => {
  let result = 0

  for (let i = 0; i < 1000000; i++) {
    result = Math.floor((result + Math.random() * 1000000)) % 1000
  }

  return result
}

let c = 0

app.get('/', async (req, res) => {
  let result

  try {
    const pool = await getPool()

    result = await pool.query(`SHOW DATABASES`)  
  } catch (e) {
    return res.status(500).send(`Error ${e}`)
  }
  
  console.log(`Responding to request #${c++} #calculation result ${calculate()}`)

  res.send(`
    <!DOCTYPE html>
    <html>
    <head>
      <title>Hello World</title>
    </head>
    <body>
      It Works! <br />
      ${JSON.stringify(result)}
    </body>
    </html>
  `)
})

app.get('/health/ready', (req, res) => {
  res.send('OK')
})

app.get('/health/live', (req, res) => {
  res.send('OK')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
