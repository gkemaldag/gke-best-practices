#!/bin/bash

kubectl apply -f cloudsql.secret.yaml
kubectl apply -f cloudsql.deployment.yaml
kubectl apply -f cloudsql.svc.yaml
kubectl apply -f configmap.yaml
